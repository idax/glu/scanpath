#!/bin/bash

BASE_DIR=$(pwd)
cd ..
mvn versions:display-plugin-updates
mvn versions:display-dependency-updates
cd $BASE_DIR
