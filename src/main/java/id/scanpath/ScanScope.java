/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.scanpath;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * This class is used to specify the classpath entries and library files to be scanned for specific
 * Java types. It also defines the name patterns to be used to filter out and arrive at the final
 * list of Java types to be used by the classpath browser for lookup operations.
 * <p>
 *
 * @author indroneel
 */

public class ScanScope {

	private List<String> classPaths;
	private List<String> libPaths;
	private List<String> includePkgs;
	private List<String> excludePkgs;
	private List<String> includeClasses;
	private List<String> excludeClasses;
	private List<String> includePats;
	private List<String> excludePats;
	private boolean      xSCP;

	public ScanScope() {
		xSCP = false;
		classPaths = new LinkedList<>();
		libPaths = new LinkedList<>();
		includePkgs = new LinkedList<>();
		excludePkgs = new LinkedList<>();
		includeClasses = new LinkedList<>();
		excludeClasses = new LinkedList<>();
		includePats = new LinkedList<>();
		excludePats = new LinkedList<>();
	}

	List<String> classPaths() {
		List<String> result = new LinkedList<>();
		if(!xSCP) {
			//Prepare the java class path for subsequent scanning.
			String javaClassPath = System.getProperty("java.class.path");
			if (javaClassPath != null) {
				for (String path : javaClassPath.split(File.pathSeparator)) {
					result.add(path);
				}
			}
		}
		result.addAll(classPaths);
		return result;
	}

	List<String> libPaths() {
		return libPaths;
	}

	List<String> includedPackages() {
		return includePkgs;
	}

	List<String> excludedPackages() {
		return excludePkgs;
	}

	List<String> includedClasses() {
		return includeClasses;
	}

	List<String> excludedClasses() {
		return excludeClasses;
	}

	List<String> includedClassesByPattern() {
		return includePats;
	}

	List<String> excludedClassesByPattern() {
		return excludePats;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Public methods

/**
 * Adds an entry to the list of classpaths that should be scanned by the classpath browser for
 * lookup of specific Java types. A classpath denotes an absolute or relative path to a folder, or a
 * ZIP/JAR archive containing a set of Java class files.
 * <p>
 *
 * @param	path a classpath entry
 * @return	this object for fluent style invocation of methods.
 */

	public ScanScope addClassPath(String path) {
		classPaths.add(path);
		return this;
	}

/**
 * Adds an entry to the list of libpaths that should be scanned by the classpath browser for lookup
 * of specific Java types. A libpath is a folder on the local file system that contains a set of ZIP
 * and JAR files, each of which should be treated as a classpath entry.
 * <p>
 *
 * @param	path a libpath entry
 * @return	this object for fluent style invocation of methods.
 */

	public ScanScope addLibPath(String path) {
		libPaths.add(path);
		return this;
	}

	public ScanScope excludeSystemClassPath(boolean flag) {
		xSCP = flag;
		return this;
	}

/**
 * Adds an entry to the list of package names to narrow down the set of Java types for lookup
 * operations by the classpath browser. Use of wildcard or any other form of pattern-matching
 * expressions are not allowed. All Java types under this packages and sub-packages are included in
 * the lookup scope.
 *
 * @param	name a Java package name for inclusion.
 * @return	this object for fluent style invocation of methods.
 */

	public ScanScope includePackage(String name) {
		includePkgs.add(name);
		return this;
	}

/**
 * Adds an entry to the list of package names to narrow down the set of Java types for lookup
 * operations by the classpath browser. Use of wildcard or any other form of pattern-matching
 * expressions are not allowed. All Java types under this packages and sub-packages are excluded
 * from the lookup scope.
 *
 * @param	name a Java package name for exclusion.
 * @return	this object for fluent style invocation of methods.
 */

	public ScanScope excludePackage(String name) {
		excludePkgs.add(name);
		return this;
	}

/** Adds an entry to the list of inclusion patterns to narrow down the set of Java types for lookup
 * operations by the classpath browser. These patterns follow the semantics of Java regular
 * expressions. Each fully qualified type name on the classpaths and libpaths is matched against
 * these patterns for inclusion in the lookup scope.
 *
 * @param	pattern a regular expression that can match a Java type name.
 * @return	this object for fluent style invocation of methods.
 */

	public ScanScope includeClassesByPattern(String pattern) {
		includePats.add(pattern);
		return this;
	}

/** Adds an entry to the list of exclusion patterns to narrow down the set of Java types for lookup
 * operations by the classpath browser. These patterns follow the semantics of Java regular
 * expressions. Each fully qualified type name on the classpaths and libpaths is matched against
 * these patterns for exclusion from the lookup scope.
 *
 * @param	pattern a regular expression that can match a Java type name.
 * @return	this object for fluent style invocation of methods.
 */

	public ScanScope excludeClassesByPattern(String pattern) {
		excludePats.add(pattern);
		return this;
	}

/**
 * Adds an entry to the list of type names to narrow down the set of Java types for lookup
 * operations by the classpath browser. Use of wildcard or any other form of pattern-matching
 * expressions are not allowed.
 *
 * @param	name a fully qualified type name for inclusion in the lookup scope.
 * @return	this object for fluent style invocation of methods.
 */

	public ScanScope includeClass(String name) {
		includeClasses.add(name);
		return this;
	}

/**
 * Adds an entry to the list of type names to narrow down the set of Java types for lookup
 * operations by the classpath browser. Use of wildcard or any other form of pattern-matching
 * expressions are not allowed.
 *
 * @param	name a fully qualified type name for exclusion from the lookup scope.
 * @return	this object for fluent style invocation of methods.
 */

	public ScanScope excludeClass(String name) {
		excludeClasses.add(name);
		return this;
	}
}
