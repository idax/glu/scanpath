/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.scanpath.support;

import java.io.IOException;
import java.io.InputStream;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 *
 * @author indroneel
 *
 */

public class JarClassFile implements ClassFile {

	private final JarFile  root;
	private final JarEntry entry;

	public JarClassFile(JarFile root, JarEntry entry) {
		this.root = root;
		this.entry = entry;
	}

	////////////////////////////////////////////////////////////////////////////
	// Methods of interface ClassFile

	@Override
	public String getName() {
		String name = entry.getName();
		if(name.startsWith("/")) {
			name = name.substring(1, name.lastIndexOf('.')).replace('/', '.');
		}
		else {
			name = name.substring(0, name.lastIndexOf('.')).replace('/', '.');
		}
		return name;
	}

	@Override
	public String getSimpleName() {
		String fqcn = getName();
		return fqcn.substring(fqcn.lastIndexOf('.') + 1);
	}

	@Override
	public InputStream openInputStream() throws IOException {
		return root.getInputStream(entry);
	}

	@Override
	public String toString() {
		return root.getName() + "!" + java.io.File.separatorChar + entry.toString();
	}
}
