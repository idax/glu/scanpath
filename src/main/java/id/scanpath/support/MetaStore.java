/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.scanpath.support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author indroneel
 *
 */

public class MetaStore {

	private static final Logger LOGGER = Logger.getLogger(MetaStore.class.getName());

	private ClassLoader              cload;
	private Map<String, Set<String>> ifaceList;
	private Map<String, Set<String>> annList;
	private Map<String, Set<String>> mthdAnnList;

	public MetaStore() {
		cload = Thread.currentThread().getContextClassLoader();
		if(cload == null) {
			cload = getClass().getClassLoader();
		}

		ifaceList = new HashMap<>();
		annList = new HashMap<>();
		mthdAnnList = new HashMap<>();
	}

	public void setClassLoader(ClassLoader cl) {
		cload = cl;
		if(cload == null) {
			cload = Thread.currentThread().getContextClassLoader();
		}
		if(cload == null) {
			cload = getClass().getClassLoader();
		}
	}

	public void addImplementation(String iface, String implementation) {
		if(ifaceList.containsKey(iface)) {
			ifaceList.get(iface).add(implementation);
		}
		else {
			HashSet<String> set = new HashSet<>();
			set.add(implementation);
			ifaceList.put(iface, set);
		}
		LOGGER.fine(String.format("interface = %s, implementation = %s", iface, implementation));
	}

	public void addClassAnnotation(String ann, String implementation) {
		if(annList.containsKey(ann)) {
			annList.get(ann).add(implementation);
		}
		else {
			HashSet<String> set = new HashSet<>();
			set.add(implementation);
			annList.put(ann, set);
		}
		LOGGER.fine(String.format("annotation = %s, implementation = %s", ann, implementation));
	}

	public void addClassWithMethodAnnotation(String ann, String implementation) {
		if(mthdAnnList.containsKey(ann)) {
			mthdAnnList.get(ann).add(implementation);
		}
		else {
			HashSet<String> set = new HashSet<>();
			set.add(implementation);
			mthdAnnList.put(ann, set);
		}
		LOGGER.fine(String.format("method_annotation = %s, implementation = %s", ann, implementation));
	}

	public List<Class<?>> listImplementingClasses(String iface) {
		List<Class<?>> result = new ArrayList<>();
		Set<String> clsNames = ifaceList.get(iface);
		if(clsNames == null) {
			return result;
		}
		for(String name : clsNames) {
			try {
				result.add(cload.loadClass(name));
			}
			catch (ClassNotFoundException exep) {
				LOGGER.log(Level.WARNING, String.format("unable to load class %s", name), exep);
			}
		}
		return result;
	}

	public List<Class<?>> listAnnotatedClasses(String ann) {
		List<Class<?>> result = new ArrayList<>();
		Set<String> clsNames = annList.get(ann);
		if(clsNames == null) {
			return result;
		}
		for(String name : clsNames) {
			try {
				result.add(cload.loadClass(name));
			}
			catch (ClassNotFoundException exep) {
				LOGGER.log(Level.WARNING, String.format("unable to load class %s", name), exep);
			}
		}
		return result;
	}

	public List<Class<?>> listClassesWithAnnotatedMethods(String ann) {
		List<Class<?>> result = new ArrayList<>();
		Set<String> clsNames = mthdAnnList.get(ann);
		if(clsNames == null) {
			return result;
		}
		for(String name : clsNames) {
			try {
				result.add(cload.loadClass(name));
			}
			catch (ClassNotFoundException exep) {
				LOGGER.log(Level.WARNING, String.format("unable to load class %s", name), exep);
			}
		}
		return result;
	}
}
