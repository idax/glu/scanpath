/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.scanpath.support;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 *
 * @author indroneel
 *
 */

public class DiskClassList implements ClassList {

	private final File root;

	public DiskClassList(File dir) {
		if (dir != null && (!dir.isDirectory() || !dir.canRead())) {
			throw new RuntimeException("cannot use dir " + dir);
		}
		root = dir;
	}

	////////////////////////////////////////////////////////////////////////////
	// Methods of interface ClassList

	@Override
	public Iterable<ClassFile> getFiles() {
		if (root == null || !root.exists()) {
			return Collections.emptyList();
		}
		Stack<File> stack = new Stack<File>();
		stack.addAll(listFiles(root));

		LinkedList<ClassFile> ll = new LinkedList<>();
		while (!stack.isEmpty()) {
			final File file = stack.pop();
			if (file.isDirectory()) {
				stack.addAll(listFiles(file));
			}
			else {
				ll.add(new DiskClassFile(root, file));
			}
		}
		return ll;
	}

	@Override
	public void close() {
		// NOOP
	}

	////////////////////////////////////////////////////////////////////////////
	// Helper methods

	private List<File> listFiles(final File file) {
		FileFilter filter = new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				if(pathname.isDirectory()) {
					return true;
				}
				if(pathname.getName().toLowerCase().endsWith(".class")) {
					return true;
				}
				return false;
			}
		};
		File[] files = file.listFiles(filter);

		if (files != null)
			return Arrays.asList(files);
		else
			return Collections.emptyList();
	}
}
