/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.scanpath.support;

import java.io.IOException;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 *
 * @author indroneel
 *
 */

public class ZipClassList implements ClassList {

	private static final Logger _L = Logger.getLogger(ZipClassList.class.getName());

	private final ZipFile zipFile;

	public ZipClassList(ZipFile file) {
		zipFile = file;
	}

	////////////////////////////////////////////////////////////////////////////
	// Methods of interface ClassArchive

	@Override
	public Iterable<ClassFile> getFiles() {
		LinkedList<ClassFile> ll = new LinkedList<>();
		Enumeration<? extends ZipEntry> entries = zipFile.entries();
		while (entries.hasMoreElements()) {
			ZipEntry entry = entries.nextElement();
			if (!entry.isDirectory()) {
				String entryName = entry.getName().toLowerCase();
				if(entryName.endsWith(".class")) {
					ll.add(new ZipClassFile(zipFile, entry));
				}
			}
		}
		return ll;
	}

	@Override
	public void close() {
		try {
			zipFile.close();
		}
		catch (IOException exep) {
			if(_L.isLoggable(Level.WARNING)) {
				_L.log(Level.WARNING, "unable to release zip file: " + zipFile.getName(), exep);
			}
		}
	}

	@Override
	public String toString() {
		return zipFile.getName();
	}
}
