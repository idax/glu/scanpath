/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package id.scanpath.support;

import java.util.logging.Level;
import java.util.logging.Logger;

import javassist.CtClass;
import javassist.NotFoundException;

/**
 *
 * @author indroneel
 *
 */

public class ImplementsVisitor extends ClassVisitor {

	private static final Logger LOGGER = Logger.getLogger(ImplementsVisitor.class.getName());

	////////////////////////////////////////////////////////////////////////////
	// Methods of base class ClassVisitor

	@Override
	public void visit(String className) {
		try {
			CtClass cls = clsPool.get(className);
			visitImpl(className, cls);
		}
		catch (NotFoundException exep) {
			LOGGER.log(Level.WARNING, String.format("unable to visit class %s", className), exep);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	// Helper methods

	private void visitImpl(String startClsName, CtClass cls) throws NotFoundException {
		CtClass supCls = cls.getSuperclass();
		if (supCls != null && !supCls.getName().equals("java.lang.Object")) {
			metaStore.addImplementation(cls.getName(), startClsName);
			visitImpl(startClsName, supCls);
		}

		CtClass[] ifaces = cls.getInterfaces();
		for (CtClass iface : ifaces) {
			visitInterfaces(startClsName, iface);
		}
	}

	private void visitInterfaces(String startClsName, CtClass iface) throws NotFoundException {
		metaStore.addImplementation(iface.getName(), startClsName);
		CtClass[] ifaces = iface.getInterfaces();
		for (CtClass niface : ifaces) {
			visitInterfaces(startClsName, niface);
		}
	}
}
