/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.scanpath.support;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * @author indroneel
 *
 */

public class ClassFilter {

	private static final Logger LOGGER = Logger.getLogger(ClassFilter.class.getName());

	private List<String> includePkgs;
	private List<String> excludePkgs;
	private List<Pattern> includeClsPatterns;
	private List<Pattern> excludeClsPatterns;
	private List<String> includeClsNames;
	private List<String> excludeClsNames;

	public ClassFilter() {
		includePkgs = new ArrayList<>();
		excludePkgs = new ArrayList<>();
		includeClsPatterns = new ArrayList<>();
		excludeClsPatterns = new ArrayList<>();
		includeClsNames = new ArrayList<>();
		excludeClsNames = new ArrayList<>();
	}

	public void includePackage(String pkgName) {
		includePkgs.add(pkgName);
	}

	public void excludePackage(String pkgName) {
		excludePkgs.add(pkgName);
	}

	public void includeClassesByPattern(String pattern) {
		try {
			includeClsPatterns.add(Pattern.compile(pattern));
		}
		catch(PatternSyntaxException exep) {
			LOGGER.log(Level.WARNING, String.format("include_class_pattern error compiling"), exep);
		}
	}

	public void excludeClassesByPattern(String pattern) {
		try {
			excludeClsPatterns.add(Pattern.compile(pattern));
		}
		catch(PatternSyntaxException exep) {
			LOGGER.log(Level.WARNING, "include_class_pattern error compiling", exep);
		}
	}

	public void includeClass(String name) {
		includeClsNames.add(name);
	}

	public void excludeClass(String name) {
		excludeClsNames.add(name);
	}

	public boolean accept(String className) {

		if(excludePkgs.isEmpty() && includePkgs.isEmpty()
				&& includeClsNames.isEmpty() && excludeClsNames.isEmpty()
				&& includeClsPatterns.isEmpty() && excludeClsPatterns.isEmpty()) {
			return true;
		}
		for(Pattern pattern : excludeClsPatterns) {
			if(pattern.matcher(className).matches()) {
				return false;
			}
		}

		for(Pattern pattern : includeClsPatterns) {
			if(pattern.matcher(className).matches()) {
				return true;
			}
		}

		for(String clsStr : excludeClsNames) {
			if(className.equals(clsStr)) {
				return false;
			}
		}

		for(String clsStr : includeClsNames) {
			if(className.equals(clsStr)) {
				return true;
			}
		}

		for(String pkgStr : excludePkgs) {
			if(className.startsWith(pkgStr + '.')) {
				return false;
			}
		}
		for(String pkgStr : includePkgs) {
			if(className.startsWith(pkgStr + '.')) {
				return true;
			}
		}
		return false;
	}
}
