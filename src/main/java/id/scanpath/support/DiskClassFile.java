/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.scanpath.support;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author indroneel
 *
 */

public class DiskClassFile implements ClassFile {

	private final File root;
	private final File file;

	public DiskClassFile(File root, File file) {
		this.root = root;
		this.file = file;
	}

	////////////////////////////////////////////////////////////////////////////
	// Methods of interface ClassFile

	@Override
	public String getName() {
		String filepath = file.getPath();
		if (filepath.startsWith(root.getPath())) {
			filepath = filepath.substring(root.getPath().length() + 1);
		}
		int endpos = filepath.lastIndexOf('.');
		return filepath.substring(0, endpos).replace(File.separatorChar, '.');
	}

	@Override
	public String getSimpleName() {
		String filepath = file.getName();
		int endpos = filepath.lastIndexOf('.');
		return filepath.substring(0, endpos);
	}

	@Override
	public InputStream openInputStream() throws IOException {
		return new FileInputStream(file);
	}
}
