/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package id.scanpath;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipFile;

import id.scanpath.support.AnnotationVisitor;
import id.scanpath.support.ClassFile;
import id.scanpath.support.ClassFilter;
import id.scanpath.support.ClassList;
import id.scanpath.support.ClassVisitor;
import id.scanpath.support.DiskClassList;
import id.scanpath.support.ImplementsVisitor;
import id.scanpath.support.JarClassList;
import id.scanpath.support.MetaStore;
import id.scanpath.support.MethodAnnotationVisitor;
import id.scanpath.support.ZipClassList;
import javassist.ClassPool;
import javassist.NotFoundException;

/**
 * @author indroneel
 *
 */

public class ClasspathScanner {

	private static final Logger LOGGER = Logger.getLogger(ClasspathScanner.class.getName());

	private List<ClassList>    classPaths;
	private List<ClassVisitor> visitors;
	private MetaStore          metaStore;
	private ClassPool          clsPool;
	private ClassFilter        clsFilter;

/**
 * Creates a new classpath scanner to search for specific items on the classpath.
 * <p>
 */

	public ClasspathScanner() {
		classPaths = new ArrayList<>();
		clsPool = new ClassPool();
		clsPool.appendSystemPath();

		visitors = new ArrayList<>();
		metaStore = new MetaStore();
		clsFilter = new ClassFilter();
	}

/**
 * Sets a custom classloader that is subsequently used to load classes from the scanned class path.
 * <p>
 *
 * @param	cl a custom classloader that is used to load the classes. A null value will default
 * 			to the system classloader.
 */

	public void setClassLoader(ClassLoader cl) {
		metaStore.setClassLoader(cl);
	}

/**
 * Prepare the list of Java types that would be further looked up for types with specific
 * characteristics and/or constructs.
 * <p>
 *
 * @param	scanp provides the configuration, in terms of classpaths and name matching criteria,
 * 			that is used to arrive at the list of Java types for further lookup.
 */

	public void load(ScanScope scanp) {
		if(scanp == null) {
			return;
		}

		for(String path : scanp.classPaths()) {
			addClassPath(path);
		}

		for(String path : scanp.libPaths()) {
			addLibPath(path);
		}

		for(String name : scanp.includedPackages()) {
			clsFilter.includePackage(name);
			LOGGER.fine(String.format("package_to_scan = %s (included)", name));
		}

		for(String name : scanp.excludedPackages()) {
			clsFilter.excludePackage(name);
			LOGGER.fine(String.format("package_to_scan = %s (excluded)", name));
		}

		for(String pattern : scanp.includedClassesByPattern()) {
			clsFilter.includeClassesByPattern(pattern);
			LOGGER.fine(String.format("class_to_scan pattern = %s (included)", pattern));
		}

		for(String pattern : scanp.excludedClassesByPattern()) {
			clsFilter.excludeClassesByPattern(pattern);
			LOGGER.fine(String.format("class_to_scan pattern = %s (excluded)", pattern));
		}

		for(String name : scanp.includedClasses()) {
			clsFilter.includeClass(name);
			LOGGER.fine(String.format("class_to_scan = %s (included)", Arrays.asList(name)));
		}

		for(String name : scanp.excludedClasses()) {
			clsFilter.excludeClass(name);
			LOGGER.fine(String.format("class_to_scan = %s (excluded)", Arrays.asList(name)));
		}

		visitors.clear();
		addClassVisitor(new ImplementsVisitor());
		addClassVisitor(new AnnotationVisitor());
		addClassVisitor(new MethodAnnotationVisitor());

		for(ClassList clsList : classPaths) {
			for(ClassFile cf : clsList.getFiles()) {
				visitClass(cf);
			}
			clsList.close();
		}

		LOGGER.info("done loading meta information from classpath");
	}

/**
 * Retrieves a list of Java types that derives/implements, either directly or indirectly, the given
 * type.
 * <p>
 *
 * @param	type a Java type that is present on the type hierarchy for each entry in the returned
 * 			list of Java types.
 * @return	the corresponding list of Java types.
 */

	public List<Class<?>> listImplementingClasses(Class<?> type) {
		return metaStore.listImplementingClasses(type.getName());
	}

/**
 * Retrieves a list of Java types, that are qualified with the given annotation. The list also
 * includes types that derive from or inherit at least one Java type that is qualified with the
 * given annotation.
 * <p>
 *
 * @param	ann the annotation, the target type being a Java class.
 * @return	the corresponding list of Java types that have the annotation, or derive/inherit from
 * 			one or more Java types that bear the annotation.
 */

	public List<Class<?>> listAnnotatedClasses(Class<?> ann) {
		return metaStore.listAnnotatedClasses(ann.getName());
	}

/**
 * Retrieves a list of Java types, that encapsulates at least one method, that is qualified with the
 * given annotation. The list also includes types that derive/inherit from at least one Java type
 * that encapsulates at least one method that is qualified with the given annotation.
 * <p>
 *
 * @param	ann the annotation, the target type being a Java method.
 * @return	the corresponding list of Java types.
 */

	public List<Class<?>> listClassesWithAnnotatedMethods(Class<?> ann) {
		return metaStore.listClassesWithAnnotatedMethods(ann.getName());
	}

	////////////////////////////////////////////////////////////////////////////
	// Helper methods

	private void addClassPath(String path) {
		File f = new File(path);
		if(f.isDirectory()) {
			classPaths.add(new DiskClassList(f));
			try {
				if(path.endsWith(File.separator)) {
					path = path.substring(0, path.length() - 1);
				}
				clsPool.appendClassPath(path);
				LOGGER.fine(String.format("path = %s (folder)", path));
			}
			catch (NotFoundException exep) {
				LOGGER.log(Level.WARNING, "error adding classpath", exep);
			}
		}
		else if(f.getName().toLowerCase().endsWith(".zip")) {
			try {
				classPaths.add(new ZipClassList(new ZipFile(f)));
				clsPool.appendClassPath(path);
				LOGGER.fine(String.format("path = %s (zipfile)", path));
			}
			catch (IOException | NotFoundException exep) {
				LOGGER.log(Level.WARNING, "error adding classpath", exep);
			}

		}
		else if(f.getName().toLowerCase().endsWith(".jar")) {
			try {
				classPaths.add(new JarClassList(new JarFile(f)));
				clsPool.appendClassPath(path);
				LOGGER.fine(String.format("path = %s (jarfile)", path));
			}
			catch (IOException | NotFoundException exep) {
				LOGGER.log(Level.WARNING, "error adding classpath", exep);
			}
		}
		else {
			LOGGER.fine(String.format("path = %s (skipped)", path));
		}
	}

	private void addLibPath(String path) {
		File pathAsFile = new File(path);
		File[] archives = pathAsFile.listFiles(new FileFilter() {
			@Override
			public boolean accept(File f) {
				String name = f.getName().toLowerCase();
				return name.endsWith(".zip") || name.endsWith(".jar");
			}
		});
		for(File f : archives) {
			addClassPath(f.getAbsolutePath());
		}
	}

	private void addClassVisitor(ClassVisitor cv) {
		cv.setMetaStore(metaStore);
		cv.setClassPool(clsPool);
		visitors.add(cv);
	}

	private void visitClass(ClassFile cf) {
		if(!clsFilter.accept(cf.getName())) {
			return;
		}
		for(ClassVisitor cv: visitors) {
			cv.visit(cf.getName());
		}
	}
}
