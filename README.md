# scanpath

Scanpath is a simple utility to traverse the Java classpath and look for specific types (classes and interfaces). This filtering is based on type name, encapsulated method signatures, and annotations that are applied on the types and methods.